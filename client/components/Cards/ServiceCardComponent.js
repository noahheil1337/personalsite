import service from '../../assets/frontpage/customerservice.png';

import React from 'react';
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const ServiceCard = () => (
  <MuiThemeProvider >
  <Card style={{width: '300px'}}>
    <CardMedia
      overlay={<CardTitle title="Overlay title" subtitle="Overlay subtitle" />}
    >
      <img src={service} alt='Customer Service Person' style={{height: '300px'}}/>
    </CardMedia>
    <CardTitle title="Customer Service" subtitle="Apparel Purchases Should Be Painless!" />
    <CardText>
      Feel free to send us an email or give us a call. We are here to help!
    </CardText>
    <CardActions>
      <FlatButton label="Action1" />
      <FlatButton label="Action2" />
    </CardActions>
  </Card>
        </MuiThemeProvider>

);

export default ServiceCard;