import shipping from '../../assets/frontpage/freeshipping.png';

import React from 'react';
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const ShippingCard = () => (
  <MuiThemeProvider>
  <Card style={{width: '300px'}}>
    <CardMedia
      overlay={<CardTitle title="Overlay title" subtitle="Overlay subtitle" />}
    >
      <img src={shipping} alt='Closed Box' style={{height: '300px'}}/>
    </CardMedia>
    <CardTitle title="Free Shipping" subtitle="Thats Just How We Treat Our Family" />
    <CardText>
      Don't ever worry about paying for shipping. We have go you covered!
    </CardText>
    <CardActions>
      <FlatButton label="Action1" />
      <FlatButton label="Action2" />
    </CardActions>
  </Card>
        </MuiThemeProvider>

);

export default ShippingCard;