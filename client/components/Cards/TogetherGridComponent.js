import React from 'react';
import { Grid, Cell } from 'react-mdl';
import BrandIdentityCard from './BrandCardComponent';
import QuickTurnAroundCard from './TurnAroundCardComponent';
import ShippingCard from './ShippingCardComponent';
import ServiceCard from './ServiceCardComponent';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';


export default class TogetherGrid extends React.Component {
  render() {
    return (
        

     <div style={{width: '80%', margin: 'auto'}}>
      <MuiThemeProvider >
      <Grid>
        <Cell col={3}><BrandIdentityCard/></Cell>
        <Cell col={3}><QuickTurnAroundCard/></Cell>
        <Cell col={3}><ShippingCard/></Cell>
        <Cell col={3}><ServiceCard/></Cell>
      </Grid>
      </MuiThemeProvider>
     </div>
    );
  }
}