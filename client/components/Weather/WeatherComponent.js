import React from 'react';
import { Grid, Cell, Textfield, Button, Checkbox } from 'react-mdl';
import Page from '../Page/PageComponent';

export default class Weather extends React.Component {
  render() {
    return (
      <Page heading='Weather'>
        <div style={{ width: '70%', margin: 'auto' }}>
          <Grid>
            <form style={{ margin: 'auto' }}>
              <Cell col={12}>
                <Textfield onChange={() => {}} label='Location' />
              </Cell>
              <Cell col={12} style={{ textAlign: 'right' }}>
                <Button primary>Search</Button>
              </Cell>
            </form>
          </Grid>
        </div>
      </Page>
    );
  }
}
