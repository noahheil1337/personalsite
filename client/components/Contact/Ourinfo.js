import React from 'react';
import styles from './Contact.scss';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText, CardMenu, Button, IconButton} from 'react-mdl';

export default class Ourinfo extends React.Component {
  render() {
    const title = 'Textile Graphix';
    return (
      <Card shadow={0} style={{width: '512px', margin: 'auto'}}>
          <CardTitle style={{color: '#fff', height: '176px', background: 'url(http://www.getmdl.io/assets/demos/welcome_card.jpg) center / cover'}}>Textile Graphix, LLC</CardTitle>
          <CardText>
            Phone: <br/>
            Email: nceheil@gmail.com
          </CardText>
          <CardActions border>
              <Button colored>Get Started</Button>
          </CardActions>
          <CardMenu style={{color: '#fff'}}>
              <IconButton name="share" />
          </CardMenu>
      </Card>
    );
  }
}
