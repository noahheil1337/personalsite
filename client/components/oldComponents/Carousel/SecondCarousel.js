import React from 'react';
import Slider from 'react-slick';
import { Grid, Cell } from 'react-mdl';
import styles from './Carousel.scss';
import embroidery from '../../assets/frontpage/embroideryicon.png';
import press from '../../assets/frontpage/press.png';

export default class SecondSlider extends React.Component {
  render() {
    var settings = {
      dots: false,
      centerMode: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div className={styles.carouselRoot}> 
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
        <Slider {...settings}>
          <div style={{width: '80%', margin: 'auto'}}>
            <Grid>
              <Cell col={6}><img style={{ width: '30%' }} src={press} alt='press shape' /></Cell>
              <Cell col={4} style={{ margin: 'auto auto auto 5%' }}><span style={{color: 'white', fontSize: '48px', lineHeight: '1', fontWeight: 'bold',}}>SCREEN PRINTING</span><br/><br/><span style={{color: 'white', fontSize: '24px', lineHeight: '1'}}>Screen printing is by far the most economical way to print a shirt. By using stenciled screens, squeegees are used to push ink through the screen and on to the garment. For larger print runs, screen printing is your best option for saving money and for providing a longer lasting product.</span></Cell>
            </Grid>
          </div>
          <div style={{width: '80%', margin: 'auto'}}>
            <Grid>
              <Cell col={3} style={{ margin: 'auto auto auto 20%' }}><span style={{color: 'white', fontSize: '48px', lineHeight: '1', fontWeight: 'bold',}}>EMBROIDERY</span><br/><br/><span style={{color: 'white', fontSize: '24px', lineHeight: '1'}}>Embroidery is most common on nicer garments used for more of a professional setting. By using thread, your logo is stitched into the garment, making it last for lightyears and beyond.</span></Cell>
              <Cell col={6}><img style={{width: '50%', margin: 'auto auto auto 1%' }} src={embroidery} alt='your logo here shirt' /></Cell>
            </Grid>
          </div>
        </Slider>
      </div>
    );
  }
}