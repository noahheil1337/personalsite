import React from 'react';
import Slider from 'react-slick';
import { Grid, Cell } from 'react-mdl';
import styles from './Carousel.scss';
import cap from '../../assets/frontpage/capicon.png';
import shirt from '../../assets/frontpage/yourlogohere.png'

export default class SSlider extends React.Component {
  render() {
    var settings = {
      dots: false,
      centerMode: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div className={styles.carouselRoot}> 
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
        <Slider {...settings}>
          <div style={{width: '80%', margin: 'auto'}}>
            <Grid>
              <Cell col={6}><img style={{ width: '60%' }} src={cap} alt='baseball cap' /></Cell>
              <Cell col={2} style={{ margin: 'auto auto auto 5%' }}><span style={{color: 'white', fontSize: '72px', lineHeight: '1', fontWeight: 'bold',}}>GUARANTEED TO COVER ANY BALD SPOT</span></Cell>
            </Grid>
          </div>
          <div style={{width: '80%', margin: 'auto'}}>
            <Grid>
              <Cell col={2} style={{ margin: 'auto auto auto 30%' }}><span style={{color: 'white', fontSize: '72px', lineHeight: '1', fontWeight: 'bold',}}>LET US CREATE YOUR APPAREL</span></Cell>
              <Cell col={6}><img style={{width: '40%', margin: 'auto auto auto 1%' }} src={shirt} alt='your logo here shirt' /></Cell>
            </Grid>
          </div>
        </Slider>
      </div>
    );
  }
}
