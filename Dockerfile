FROM node:7

RUN apt-get update && apt-get install -y --no-install-recommends \
		vim \
                wget \
                libxml2-dev \
                libxslt1-dev \
                libpq-dev \
                supervisor \
                ca-certificates \
                net-tools \
		postgresql-client \
		g++ \
		gcc \
                bash \
		libc6-dev \
		make \
		curl \
		git \
		mercurial \
		binutils \
		bison \
		build-essential \
		pkg-config \
		apt-transport-https \
		&& rm -rf /var/lib/apt/lists/*
 
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update && apt-get install yarn

COPY ./ /usr/src/app/

WORKDIR /usr/src/app/

RUN yarn

RUN npm install

CMD npm start
